# Álcool ou Gasolina

O aplicativo constuido com Flutter, um framework desenvolvido pelo Google na linguagem Dart.
Objetivo: calcula r e fazer um comparativo de qual dos dois combustíveis é mais vantajoso de utilizar.

## Organização

A aplicação esta inteiramente componentizada, facilitando o reuso de widgets. Dentre os widgets:

* Input: trata a entrada de dado e onde há aplicação de mascara aos dados
* Loading: animação de carregamento
* Submit form : onde são chamados os componentes de inputs e button
* Success: mensagem de retorno ao finalizar cada consulta

Em pages há:

* Home: onde é feita a chama a classe a Success e SubmitForm e ocorre o tratamento de exibição, além disso conta com as funçẽos de calculo e reset.
