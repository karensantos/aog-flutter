import 'package:flutter/material.dart';

class LoadingButton extends StatelessWidget {
  var busy = false;
  var invert = false;
  Function calc;
  var text ="";

  LoadingButton({
    @required this.busy,
    @required this.calc,
    @required this.invert,
    @required this.text
  });

  @override
  Widget build(BuildContext context) {
    return busy
        ? Container(
          alignment: Alignment.center,
          height: 50,
          child: CircularProgressIndicator(
            backgroundColor: Colors.white,
          ),
        )
    : Container(
      height: 60,
      width: double.infinity,
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: invert ?Theme.of(context).primaryColor : Colors.white.withOpacity(0.8),
          borderRadius: BorderRadius.circular(60)),
      child: FlatButton(
        child: Text(
          text,
          style: TextStyle(
              color: invert ? Colors.white : Theme.of(context).primaryColor,
              fontFamily: "Big Shoulders Display",
              fontSize: 25),
        ),
        onPressed: calc,
      ),
    );
  }
}
